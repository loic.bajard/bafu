
$(document).ready(function() {
/*$.pageLoader();*/
/*$.fancybox.open([
	{
		href : 'http://media02.hongkiat.com/flat-design-resources/web-browser-flat-icons-freebie.jpg?new',
		title : 'Des problèmes pour visualiser le site ? Mettez à jour votre naviguateur :)',
	},			

], {
	helpers : {
		thumbs : {
			width: 75,
			height: 50
		}
	}
});*/
var windowHeight= $(window).height();
if(windowHeight < 700){
	$( "#urbanismeB" ).removeClass( "col-md-3 col-md-pull-6" ).addClass( "col-md-3 col-md-pull-8" );
	$( "#tourneB" ).removeClass( "col-md-3 col-md-pull-6" ).addClass( "col-md-3 col-md-pull-9" );
	$( "#fondreB" ).removeClass( "col-md-3 col-md-pull-6" ).addClass( "col-md-3 col-md-pull-9" );
	$( "#fondreBis" ).removeClass( "col-md-6 col-md-push-1" ).addClass( "col-md-7 col-md-push-1" );
	$( "#placeB" ).removeClass( "col-md-3 col-md-pull-7" ).addClass( "col-md-3 col-md-pull-10" );
	$( "#placeBis" ).removeClass( "col-md-4 col-md-push-1" ).addClass( "col-md-5" );
}
var windowWidth= $(window).width();
if(windowWidth < 769){
	$( "#imgEth" ).attr("src", "imgs/ethiqueR.png");
	$( "#imgMat" ).attr("src", "imgs/materiauxR.png");
	$( "#imgRen" ).attr("src", "imgs/rendreR.png");
	$( "#imgPerm" ).attr("src", "imgs/permeabiliteR.png");
	$( "#imgAcc" ).attr("src", "imgs/accompagnerR.png");
	$( "#imgInte" ).attr("src", "imgs/integrerR.png");
}
$('#materiauxQuoteIntro').hide();
$('#permQuoteIntro').hide();
$('#integrerQuoteIntro').hide();
$("#galerie3").hide();
$("#galerie1").show();
$("#galerie4").hide();
$("#galerie5").hide();
$("#galerie6").hide();
$("#galerie2").hide();
$("#bafuIntro").hide();
$('#bafuIntro').delay(1000).fadeIn(2000);
/* Initialisation + animations ------------------------------------------------------------------------------ */
	$('#fullpage').fullpage({
		anchors: ['bafu', 'circulation', 'espacePublic', 'hydraulique', 'urbanisme', 'paysage'],
		menu: '#menu',
		scrollingSpeed: 500,
		scrollOverflow:true,
		loopBottom:true,
		loopTop:true,
		verticalCentered: true,
		touchSensitivity:15,

		afterLoad: function(anchorLink, index){
	        if(anchorLink == 'bafu'){
	        	$('#bafuIntro').fadeIn(2000);
				$("#galerie3").hide();
				$("#galerie1").show();
				$("#galerie4").hide();
				$("#galerie5").hide();
				$("#galerie6").hide();
				$("#galerie2").hide();
				$('#left span').text('');
				
	        }else if(anchorLink == 'circulation'){
	        	$("#galerie3").hide();
	  			$("#galerie1").hide();
				$("#galerie4").hide();
				$("#galerie5").hide();
				$("#galerie6").hide();
				$("#galerie2").show();
	        	$('#circuIntro').delay(10).animate({top: '0%'}, 1500, 'easeOutExpo');
	        	$('#left span').text('DIJON - Rond point de la nation');
	 
	        }else if(anchorLink == 'espacePublic'){
	        	$('#espaceIntro').delay(10).animate({top: '0%'}, 1500, 'easeOutExpo');
	        	$('#left span').text('AHUY - Place du 19 mars 1962');
	        	$("#galerie3").show();
	  			$("#galerie1").hide();
				$("#galerie2").hide();
				$("#galerie4").hide();
				$("#galerie5").hide();
				$("#galerie6").hide();
	        
	        }else if(anchorLink == 'hydraulique'){
	        	$('#hydrauIntro').delay(10).animate({top: '0%'}, 1500, 'easeOutExpo');
	        	$('#left span').text('');
	      		$("#galerie4").show();
	  			$("#galerie1").hide();
				$("#galerie2").hide();
				$("#galerie3").hide();
				$("#galerie5").hide();
				$("#galerie6").hide();
				
	        }else if(anchorLink == 'urbanisme'){
	        	$('#urbaIntro').delay(10).animate({top: '0%'}, 1500, 'easeOutExpo');
	        	$('#left span').text('IS SUR TILLE - Le Prevert');
	        	$("#galerie5").show();
	  			$("#galerie1").hide();
				$("#galerie2").hide();
				$("#galerie3").hide();
				$("#galerie4").hide();
				$("#galerie6").hide();
	        
	        }else if(anchorLink == 'paysage'){
	        	$('#paysageIntro').delay(10).animate({top: '0%'}, 1500, 'easeOutExpo');
	        	$('#left span').text('FONTAINE LES DIJON - Les Champs Remy');
	            $("#galerie6").show();
	  			$("#galerie1").hide();
				$("#galerie2").hide();
				$("#galerie3").hide();
				$("#galerie4").hide();
				$("#galerie5").hide();
			
	        }
	    },
	    onLeave: function(index, nextIndex, direction){
	    	if(index =='1')
	    	{
	    		if(direction == 'down')
	    		{
	    			$('#bafuIntro').hide();
	    		}
	    		else if(direction =='up')
	    		{
	    			$('#bafuIntro').hide();
	    		}
	    	}
	    	else if(index == '2')
	    	{
	    		if(direction == 'down')
	    		{
					$('#circuIntro').delay(10).animate({top: '-130%'}, 1500, 'easeOutExpo');
	    		}
	    		else if(direction =='up')
	    		{
					$('#circuIntro').delay(10).animate({top: '-130%'}, 1500, 'easeOutExpo');

	    		}
	    	}
	    	else if(index == '3')
	    	{
	    		if(direction == 'down')
	    		{
	    			$('#espaceIntro').delay(10).animate({top: '-130%'}, 1500, 'easeOutExpo');
	    		}
	    		else if(direction =='up')
	    		{
	    			$('#espaceIntro').delay(10).animate({top: '-130%'}, 1500, 'easeOutExpo');
	    		}
	    	}
	    	else if(index == '4')
	    	{
	    		if(direction == 'down')
	    		{
	    			$('#hydrauIntro').delay(10).animate({top: '-130%'}, 1500, 'easeOutExpo');
	    		}
	    		else if(direction =='up')
	    		{
	    			$('#hydrauIntro').delay(10).animate({top: '-130%'}, 1500, 'easeOutExpo');
	    		}
	    	}
	    	else if(index == '5')
	    	{
	    		if(direction == 'down')
	    		{
	    			$('#urbaIntro').delay(10).animate({top: '-130%'}, 1500, 'easeOutExpo');
	    		}
	    		else if(direction =='up')
	    		{
	    			$('#urbaIntro').delay(10).animate({top: '-130%'}, 1500, 'easeOutExpo');
	    		}
	    	}
	    	else if(index == '6')
	    	{
	    		if(direction == 'down')
	    		{
	    			$('#paysageIntro').delay(10).animate({top: '-130%'}, 1500, 'easeOutExpo');
	    		}
	    		else if(direction =='up')
	    		{
					$('#paysageIntro').delay(10).animate({top: '-130%'}, 1500, 'easeOutExpo');
	    		}
	    	}
        },
	    afterSlideLoad: function( anchorLink, index, slideAnchor, slideIndex)
	    {
		    if(anchorLink == 'bafu')
		    {
		    	if(slideIndex == 1)
		    	{
	 			 	$('#ancienIntro').css('visibility', 'visible');
	 			 	$('#ancienIntro').delay(10).animate({right: '0%'}, 1500, 'easeOutExpo');
	 			 	$('#left span').text('');
		    	}
		    	else if(slideIndex == 2)
		    	{
			    	$('#pluriIntro').css('visibility', 'visible');
			    	$('#pluriBIntro').css('visibility', 'visible');
			    	$('#pluriIntro').delay(20).animate({right: '0%'}, 1500, 'easeOutExpo');
			    	$('#pluriBIntro').delay(10).animate({left: '0%'}, 1500, 'easeOutExpo');
			    	$('#left span').text('');
   				}
   				else if(slideIndex == 3)
   				{
   					$('#proxiIntro').css('visibility', 'visible');
	 			 	$('#proxiIntro').delay(100).animate({left: '0%'}, 1500, 'easeOutExpo');
					$('#bourgogneIntro').css('visibility', 'visible');
	 			 	$('#bourgogneIntro').delay(450).animate({left: '0%'}, 1500, 'easeOutExpo');
					$('#coteIntro').css('visibility', 'visible');
					$('#coteIntro').delay(450).animate({right: '0%'}, 1500, 'easeOutExpo');
					$('#left span').text('');
   				}
   				else if(slideIndex == 4)
   				{
   					$('#ethiqueIntro').css('visibility', 'visible');
	 			 	$('#ethiqueIntro').delay(100).animate({left: '0%'}, 1500, 'easeOutExpo');
	 			 	$('#left span').text('');
   				}
		    
		    } 
		    else if(anchorLink == 'circulation')
		    {
		    	if(slideIndex == 1)
		    	{
	 			    $('#giratoireIntro').css('visibility', 'visible');
			    	$('#tourneIntro').css('visibility', 'visible');
			    	$('#giratoireIntro').delay(20).animate({right: '0%'}, 1500, 'easeOutExpo');
			    	$('#tourneIntro').delay(10).animate({left: '0%'}, 1500, 'easeOutExpo');
			    	$('#left span').text('');
		    	}
		    	else if(slideIndex == 2)
		    	{
					$('#amenagementIntro').css('visibility', 'visible');
	 			 	$('#amenagementIntro').delay(100).animate({left: '0%'}, 1500, 'easeOutExpo');
	 			 	$('#left span').text('ARCELOT - RD 960');
   				}
   				else if(slideIndex == 3)
   				{
	 			 	$('#voirieIntro').css('visibility', 'visible');
	 			 	$('#voirieIntro').delay(100).animate({right: '0%'}, 1500, 'easeOutExpo');
	 			 	$('#left span').text('QUINCY LE VICOMTE - Grande Ru');

   				}
   				else if(slideIndex == 4)
   				{
	 			 	$('#securiteIntro').css('visibility', 'visible');
	 			 	$('#securiteIntro').delay(100).animate({left: '0%'}, 1500, 'easeOutExpo');
	 			 	$('#left span').text('PERRIGNY LES DIJON - Rue de Vignery');
   				}
   				else if(slideIndex == 5)
   				{
   					$('#traitementIntro').css('visibility', 'visible');
	 			 	$('#traitementIntro').delay(100).animate({right: '0%'}, 1500, 'easeOutExpo');
	 			 	$('#left span').text('');
   				}
   				else if(slideIndex == 6)
   				{
   					$('#pisteIntro').css('visibility', 'visible');
	 			 	$('#pisteIntro').delay(100).animate({left: '0%'}, 1500, 'easeOutExpo');
	 			 	$('#left span').text('CHEVIGNY SAINT SAUVEUR - ZAC Terres Rousses');
   				}
   				else if(slideIndex == 7)
   				{
   					$('#tramwayIntro').css('visibility', 'visible');
	 			 	$('#tramwayIntro').delay(100).animate({right: '0%'}, 1500, 'easeOutExpo');
	 			 	$('#left span').text('DIJON - Tramway Ligne T2');
   				}
   				else if(slideIndex == 8)
   				{
   					$('#handicapIntro').css('visibility', 'visible');
	 			 	$('#handicapIntro').delay(100).animate({left: '0%'}, 1500, 'easeOutExpo');
	 			 	$('#left span').text('DIJON - Avenue du Drapeau');
   				}
   				else if(slideIndex == 9)
   				{
   					$('#materiauxIntro').css('visibility', 'visible');
	 			 	$('#materiauxIntro').delay(100).animate({left: '0%'}, 1500, 'easeOutExpo');
   					$('#materiauxQuoteIntro').delay(350).fadeIn(1500);
   					$('#left span').text('');
   				}
		    
		    }
		    else if(anchorLink == 'espacePublic')
		    {
		    	if(slideIndex == 1)
		    	{
	 			 	$('#placeIntro').css('visibility', 'visible');
	 			 	$('#placeIntro').delay(10).animate({left: '0%'}, 1500, 'easeOutExpo');
	 			 	$('#left span').text('GEVREY CHAMBERTIN - Place du Château');
		    	}
		    	else if(slideIndex == 2)
		    	{
			    	$('#parvisIntro').css('visibility', 'visible');
			    	$('#parvisIntro').delay(10).animate({right: '0%'}, 1500, 'easeOutExpo');
			    	$('#left span').text('CRIMOLOIS - Parvis du Monument aux morts');
   				}
   				else if(slideIndex == 3)
   				{
   					$('#mailIntro').css('visibility', 'visible');
	 			 	$('#mailIntro').delay(100).animate({left: '0%'}, 1500, 'easeOutExpo');
	 			 	$('#left span').text('DIJON - Le Clos de Pouilly');
   				}
   				else if(slideIndex == 4)
   				{
   					$('#eclairageIntro').css('visibility', 'visible');
			    	$('#eclairageIntro').delay(10).animate({right: '0%'}, 1500, 'easeOutExpo');
			    	$('#left span').text('');
   				}
   				else if(slideIndex == 5)
   				{
   					$('#rendreIntro').css('visibility', 'visible');
	 			 	$('#rendreIntro').delay(100).animate({left: '0%'}, 1500, 'easeOutExpo');
	 			 	$('#left span').text('');
   				}
		    
		    }
		    else if(anchorLink == 'hydraulique')
		    {
		    	if(slideIndex == 1)
		    	{
	 			 	$('#noueIntro').css('visibility', 'visible');
	 			 	$('#noueIntro').delay(10).animate({left: '0%'}, 1500, 'easeOutExpo');
	 			 	$('#left span').text('GEVREY CHAMBERTIN - ZAC BERGIS');
		    	}
		    	else if(slideIndex == 2)
		    	{
			  		$('#bassinIntro').css('visibility', 'visible');
	 			 	$('#bassinIntro').delay(10).animate({right: '0%'}, 1500, 'easeOutExpo');
	 			 	$('#left span').text('');
   				}
   				else if(slideIndex == 3)
   				{
   					$('#espaceVertIntro').css('visibility', 'visible');
	 			 	$('#espaceVertIntro').delay(100).animate({left: '0%'}, 1500, 'easeOutExpo');
	 			 	$('#left span').text('CHENOVE - Ilot Renan');
   				}
   				else if(slideIndex == 4)
   				{
   					$('#favoriserIntro').css('visibility', 'visible');
	 			 	$('#favoriserIntro').delay(100).animate({right: '0%'}, 1500, 'easeOutExpo');
	 			 	$('#left span').text('CHENOVE - Ilot Renan');
   				}
   				else if(slideIndex == 5)
   				{
   					$('#missionIntro').css('visibility', 'visible');
	 			 	$('#missionIntro').delay(10).animate({left: '0%'}, 1500, 'easeOutExpo');
	 			 	$('#left span').text('');
   				}
   				else if(slideIndex == 6)
   				{
   					$('#permIntro').css('visibility', 'visible');
	 			 	$('#permIntro').delay(100).animate({left: '0%'}, 1500, 'easeOutExpo');
	 			 	$('#permQuoteIntro').delay(350).fadeIn(1500);
	 			 	$('#left span').text('');
   				}
		    
		    }
		    else if(anchorLink == 'urbanisme')
		    {
		    	if(slideIndex == 1)
		    	{
	 			 	$('#concepteurIntro').css('visibility', 'visible');
	 			 	$('#concepteurIntro').delay(10).animate({left: '0%'}, 1500, 'easeOutExpo');
	 			 	$('#left span').text('');
		    	}
		    	else if(slideIndex == 2)
		    	{
					$('#monteurIntro').css('visibility', 'visible');
	 			 	$('#monteurIntro').delay(10).animate({right: '0%'}, 1500, 'easeOutExpo');
	 			 	$('#left span').text('');
   				}
   				else if(slideIndex == 3)
   				{
   					$('#procedureIntro').css('visibility', 'visible');
	 			 	$('#procedureIntro').delay(10).animate({left: '0%'}, 1500, 'easeOutExpo');
	 			 	$('#procedureIcon').css('visibility', 'visible');
	 			 	$('#procedureIcon').delay(30).animate({bottom: '0%'}, 1500, 'easeOutExpo');
	 			 	$('#left span').text('');
   				}
   				else if(slideIndex == 4)
   				{
   					$('#assistanceIntro').css('visibility', 'visible');
	 			 	$('#assistanceIntro').delay(10).animate({right: '0%'}, 1500, 'easeOutExpo');
	 			 	$('#left span').text('');
   				}
   				else if(slideIndex == 5)
   				{
   					$('#imaginaireIntro').css('visibility', 'visible');
	 			 	$('#imaginaireIntro').delay(10).animate({left: '0%'}, 1500, 'easeOutExpo');
	 			 	$('#left span').text('');
   				}
		    }
		    else if(anchorLink == 'paysage')
		    {
		    	if(slideIndex == 1)
		    	{
	 			 	$('#glisserTIntro').css('visibility', 'visible');
			    	$('#glisserBIntro').css('visibility', 'visible');
			    	$('#glisserTIntro').delay(20).animate({right: '0%'}, 1500, 'easeOutExpo');
			    	$('#glisserBIntro').delay(10).animate({left: '0%'}, 1500, 'easeOutExpo');
			    	$('#left span').text('');
		    	}
		    	else if(slideIndex == 2)
		    	{
			    	$('#alternanceIntro').css('visibility', 'visible');
	 			 	$('#alternanceIntro').delay(100).animate({right: '0%'}, 1500, 'easeOutExpo');
	 			 	$('#left span').text('GEVREY CHAMBERTIN - Place du Château');
   				}
   				else if(slideIndex == 3)
   				{
   					$('#detailTIntro').css('visibility', 'visible');
			    	$('#detailBIntro').css('visibility', 'visible');
			    	$('#detailTIntro').delay(20).animate({right: '0%'}, 1500, 'easeOutExpo');
			    	$('#detailBIntro').delay(10).animate({left: '0%'}, 1500, 'easeOutExpo');
			    	$('#left span').text('');
   				}
   				else if(slideIndex == 4)
   				{
   					$('#plantationIntro').css('visibility', 'visible');
	 			 	$('#plantationIntro').delay(100).animate({right: '0%'}, 1500, 'easeOutExpo');
	 			 	$('#left span').text('FONTAINE LES DIJON - Les Champs Remy');
   				}
   				else if(slideIndex == 5)
   				{
   					$('#integrerIntro').css('visibility', 'visible');
	 			 	$('#integrerIntro').delay(100).animate({left: '0%'}, 1500, 'easeOutExpo');
	 			 	$('#integrerQuoteIntro').delay(350).fadeIn(1500);
	 			 	$('#left span').text('');
   				}
		    }
		},
		onSlideLeave: function( anchorLink, index, slideIndex, direction){
            
			if(anchorLink == 'bafu')
			{
				if(slideIndex == 1)
				{
					if(direction == 'right')
					{
						$('#ancienIntro').animate({right: '-130%'}, 1500, 'easeOutExpo');
            			$('#ancienIntro').css('visibility', 'hidden');
					}
					else if(direction == 'left')
					{
						$('#ancienIntro').animate({right: '-130%'}, 1500, 'easeOutExpo');
       					$('#ancienIntro').css('visibility', 'hidden');
					}
				}
				else if(slideIndex == 2)
				{
					if(direction == 'right')
					{
						$('#pluriIntro').animate({right: '-130%'}, 1500, 'easeOutExpo');
                		$('#pluriBIntro').animate({left: '-130%'}, 1500, 'easeOutExpo');
           				$('#pluriIntro').css('visibility', 'hidden');
           				$('#pluriBIntro').css('visibility', 'hidden');
					}
					else if(direction == 'left')
					{
						$('#pluriIntro').animate({right: '-130%'}, 1500, 'easeOutExpo');
           				$('#pluriBIntro').animate({left: '-130%'}, 1500, 'easeOutExpo');
           				$('#pluriIntro').css('visibility', 'hidden');
           				$('#pluriBIntro').css('visibility', 'hidden');
					}
				}
				else if(slideIndex == 3)
				{
					if(direction == 'right')
					{
	 			 		$('#proxiIntro').animate({left: '-130%'}, 1500, 'easeOutExpo');
						$('#proxiIntro').css('visibility', 'hidden');
						$('#bourgogneIntro').animate({left: '-130%'}, 1500, 'easeOutExpo');
						$('#bourgogneIntro').css('visibility', 'hidden');
						$('#coteIntro').animate({right: '-130%'}, 1500, 'easeOutExpo');
						$('#coteIntro').css('visibility', 'hidden');
					}
					else if(direction == 'left')
					{
						$('#proxiIntro').animate({left: '-130%'}, 1500, 'easeOutExpo');
						$('#proxiIntro').css('visibility', 'hidden');
						$('#bourgogneIntro').animate({left: '-130%'}, 1500, 'easeOutExpo');
						$('#bourgogneIntro').css('visibility', 'hidden');
						$('#coteIntro').animate({right: '-130%'}, 1500, 'easeOutExpo');
						$('#coteIntro').css('visibility', 'hidden');
					}
				}
				else if(slideIndex == 4)
				{
					if(direction == 'right')
					{
	 			 		$('#ethiqueIntro').animate({left: '-130%'}, 1500, 'easeOutExpo');
   						$('#ethiqueIntro').css('visibility', 'hidden');
					}
					else if(direction == 'left')
					{
						$('#ethiqueIntro').animate({left: '-130%'}, 1500, 'easeOutExpo');
   						$('#ethiqueIntro').css('visibility', 'hidden');
					}
				}
			}
			else if(anchorLink == 'circulation')
			{
				if(slideIndex == 1)
				{
					if(direction == 'right')
					{
			    		$('#tourneIntro').animate({left: '-130%'}, 1500, 'easeOutExpo');
			    		$('#giratoireIntro').animate({right: '-130%'}, 1500, 'easeOutExpo');
						$('#giratoireIntro').css('visibility', 'hidden');
			    		$('#tourneIntro').css('visibility', 'hidden');
					}
					else if(direction == 'left')
					{
						$('#tourneIntro').animate({left: '-130%'}, 1500, 'easeOutExpo');
			    		$('#giratoireIntro').animate({right: '-130%'}, 1500, 'easeOutExpo');
						$('#giratoireIntro').css('visibility', 'hidden');
			    		$('#tourneIntro').css('visibility', 'hidden');
					}
				}
				else if(slideIndex == 2)
				{
					if(direction == 'right')
					{
	 			 		$('#amenagementIntro').animate({left: '-130%'}, 1500, 'easeOutExpo');
						$('#amenagementIntro').css('visibility', 'hidden');
					}
					else if(direction == 'left')
					{
	 			 		$('#amenagementIntro').animate({left: '-130%'}, 1500, 'easeOutExpo');
						$('#amenagementIntro').css('visibility', 'hidden');
					}
				}
				else if(slideIndex == 3)
				{
					if(direction == 'right')
					{
						$('#voirieIntro').animate({right: '-130%'}, 1500, 'easeOutExpo');
						$('#voirieIntro').css('visibility', 'hidden');
					}
					else if(direction == 'left')
					{
						$('#voirieIntro').animate({right: '-130%'}, 1500, 'easeOutExpo');
						$('#voirieIntro').css('visibility', 'hidden');
					}
				}
				else if(slideIndex == 4)
				{
					if(direction == 'right')
					{
						$('#securiteIntro').animate({left: '-130%'}, 1500, 'easeOutExpo');
						$('#securiteIntro').css('visibility', 'hidden');
					}
					else if(direction == 'left')
					{
						$('#securiteIntro').animate({left: '-130%'}, 1500, 'easeOutExpo');
						$('#securiteIntro').css('visibility', 'hidden');
					}
				}
				else if(slideIndex == 5)
				{
					if(direction == 'right')
					{
						$('#traitementIntro').animate({right: '-130%'}, 1500, 'easeOutExpo');
						$('#traitementIntro').css('visibility', 'hidden');
					}
					else if(direction == 'left')
					{
						$('#traitementIntro').animate({right: '-130%'}, 1500, 'easeOutExpo');
						$('#traitementIntro').css('visibility', 'hidden');
					}
				}
				else if(slideIndex == 6)
				{
					if(direction == 'right')
					{
						$('#pisteIntro').animate({left: '-130%'}, 1500, 'easeOutExpo');
						$('#pisteIntro').css('visibility', 'hidden');
					}
					else if(direction == 'left')
					{
						$('#pisteIntro').animate({left: '-130%'}, 1500, 'easeOutExpo');
						$('#pisteIntro').css('visibility', 'hidden');
					}
				}
				else if(slideIndex == 7)
				{
					if(direction == 'right')
					{
						$('#tramwayIntro').animate({right: '-130%'}, 1500, 'easeOutExpo');
						$('#tramwayIntro').css('visibility', 'hidden');
					}
					else if(direction == 'left')
					{
						$('#tramwayIntro').animate({right: '-130%'}, 1500, 'easeOutExpo');
						$('#tramwayIntro').css('visibility', 'hidden');
					}
				}
				else if(slideIndex == 8)
				{
					if(direction == 'right')
					{
						$('#handicapIntro').animate({left: '-130%'}, 1500, 'easeOutExpo');
						$('#handicapIntro').css('visibility', 'hidden');
					}
					else if(direction == 'left')
					{
						$('#handicapIntro').animate({left: '-130%'}, 1500, 'easeOutExpo');
						$('#handicapIntro').css('visibility', 'hidden');
					}
				}
				else if(slideIndex == 9)
				{
					if(direction == 'right')
					{
						$('#materiauxIntro').animate({left: '-130%'}, 1500, 'easeOutExpo');
						$('#materiauxIntro').css('visibility', 'hidden');
						$('#materiauxQuoteIntro').hide();
					}
					else if(direction == 'left')
					{
						$('#materiauxIntro').animate({left: '-130%'}, 1500, 'easeOutExpo');
						$('#materiauxIntro').css('visibility', 'hidden');
						$('#materiauxQuoteIntro').hide();
					}
				}
			}
			else if(anchorLink == 'espacePublic')
			{
				if(slideIndex == 1)
				{
					if(direction == 'right')
					{
	 			 		$('#placeIntro').animate({left: '-130%'}, 1500, 'easeOutExpo');
						$('#placeIntro').css('visibility', 'hidden');
					}
					else if(direction == 'left')
					{
	 			 		$('#placeIntro').animate({left: '-130%'}, 1500, 'easeOutExpo');
						$('#placeIntro').css('visibility', 'hidden');
					}
				}
				else if(slideIndex == 2)
				{
					if(direction == 'right')
					{
	 			 		$('#parvisIntro').animate({right: '-130%'}, 1500, 'easeOutExpo');
						$('#parvisIntro').css('visibility', 'hidden');
					}
					else if(direction == 'left')
					{
	 			 		$('#parvisIntro').animate({right: '-130%'}, 1500, 'easeOutExpo');
						$('#parvisIntro').css('visibility', 'hidden');
					}
				}
				else if(slideIndex == 3)
				{
					if(direction == 'right')
					{
	 			 		$('#mailIntro').animate({left: '-130%'}, 1500, 'easeOutExpo');
						$('#mailIntro').css('visibility', 'hidden');
					}
					else if(direction == 'left')
					{
	 			 		$('#mailIntro').animate({left: '-130%'}, 1500, 'easeOutExpo');
						$('#mailIntro').css('visibility', 'hidden');
					}
				}
				else if(slideIndex == 4)
				{
					if(direction == 'right')
					{
	 			 		$('#eclairageIntro').animate({right: '-130%'}, 1500, 'easeOutExpo');
						$('#eclairageIntro').css('visibility', 'hidden');
					}
					else if(direction == 'left')
					{
	 			 		$('#eclairageIntro').animate({right: '-130%'}, 1500, 'easeOutExpo');
						$('#eclairageIntro').css('visibility', 'hidden');
					}
				}
				else if(slideIndex == 5)
				{
					if(direction == 'right')
					{
	 			 		$('#rendreIntro').animate({left: '-130%'}, 1500, 'easeOutExpo');
						$('#rendreIntro').css('visibility', 'hidden');
					}
					else if(direction == 'left')
					{
	 			 		$('#rendreIntro').animate({left: '-130%'}, 1500, 'easeOutExpo');
						$('#rendreIntro').css('visibility', 'hidden');
					}
				}
			}
			else if(anchorLink == 'hydraulique')
			{
				if(slideIndex == 1)
				{
					if(direction == 'right')
					{
	 			 		$('#noueIntro').animate({left: '-130%'}, 1500, 'easeOutExpo');
						$('#noueIntro').css('visibility', 'hidden');
					}
					else if(direction == 'left')
					{
	 			 		$('#noueIntro').animate({left: '-130%'}, 1500, 'easeOutExpo');
						$('#noueIntro').css('visibility', 'hidden');
					}
				}
				else if(slideIndex == 2)
				{
					if(direction == 'right')
					{
	 			 		$('#bassinIntro').animate({right: '-130%'}, 1500, 'easeOutExpo');
						$('#bassinIntro').css('visibility', 'hidden');
					}
					else if(direction == 'left')
					{
	 			 		$('#bassinIntro').animate({right: '-130%'}, 1500, 'easeOutExpo');
						$('#bassinIntro').css('visibility', 'hidden');
					}
				}
				else if(slideIndex == 3)
				{
					if(direction == 'right')
					{
	 			 		$('#espaceVertIntro').animate({left: '-130%'}, 1500, 'easeOutExpo');
						$('#espaceVertIntro').css('visibility', 'hidden');
					}
					else if(direction == 'left')
					{
	 			 		$('#espaceVertIntro').animate({left: '-130%'}, 1500, 'easeOutExpo');
						$('#espaceVertIntro').css('visibility', 'hidden');
					}
				}
				else if(slideIndex == 4)
				{
					if(direction == 'right')
					{
	 			 		$('#favoriserIntro').animate({right: '-130%'}, 1500, 'easeOutExpo');
						$('#favoriserIntro').css('visibility', 'hidden');
					}
					else if(direction == 'left')
					{
	 			 		$('#favoriserIntro').animate({right: '-130%'}, 1500, 'easeOutExpo');
						$('#favoriserIntro').css('visibility', 'hidden');
					}
				}
				else if(slideIndex == 5)
				{
					if(direction == 'right')
					{
	 			 		$('#missionIntro').animate({left: '-130%'}, 1500, 'easeOutExpo');
						$('#missionIntro').css('visibility', 'hidden');
					}
					else if(direction == 'left')
					{
	 			 		$('#missionIntro').animate({left: '-130%'}, 1500, 'easeOutExpo');
						$('#missionIntro').css('visibility', 'hidden');
					}
				}
				else if(slideIndex == 6)
				{
					if(direction == 'right')
					{
	 			 		$('#permIntro').animate({left: '-130%'}, 1500, 'easeOutExpo');
						$('#permIntro').css('visibility', 'hidden');
						$('#permQuoteIntro').hide();
					}
					else if(direction == 'left')
					{
	 			 		$('#permIntro').animate({left: '-130%'}, 1500, 'easeOutExpo');
						$('#permIntro').css('visibility', 'hidden');
						$('#permQuoteIntro').hide();
					}
				}
			}
			else if(anchorLink == 'urbanisme')
			{
				if(slideIndex == 1)
				{
					if(direction == 'right')
					{
	 			 		$('#concepteurIntro').animate({left: '-130%'}, 1500, 'easeOutExpo');
						$('#concepteurIntro').css('visibility', 'hidden');
					}
					else if(direction == 'left')
					{
	 			 		$('#concepteurIntro').animate({left: '-130%'}, 1500, 'easeOutExpo');
						$('#concepteurIntro').css('visibility', 'hidden');
					}
				}
				else if(slideIndex == 2)
				{
					if(direction == 'right')
					{
	 			 		$('#monteurIntro').animate({right: '-130%'}, 1500, 'easeOutExpo');
						$('#monteurIntro').css('visibility', 'hidden');
					}
					else if(direction == 'left')
					{
	 			 		$('#monteurIntro').animate({right: '-130%'}, 1500, 'easeOutExpo');
						$('#monteurIntro').css('visibility', 'hidden');
					}
				}
				else if(slideIndex == 3)
				{
					if(direction == 'right')
					{
	 			 		$('#procedureIntro').animate({left: '-130%'}, 1500, 'easeOutExpo');
						$('#procedureIntro').css('visibility', 'hidden');
						$('#procedureIcon').animate({bottom: '-130%'}, 1500, 'easeOutExpo');
						$('#procedureIcon').css('visibility', 'hidden');
					}
					else if(direction == 'left')
					{
	 			 		$('#procedureIntro').animate({left: '-130%'}, 1500, 'easeOutExpo');
						$('#procedureIntro').css('visibility', 'hidden');
						$('#procedureIcon').animate({bottom: '-130%'}, 1500, 'easeOutExpo');
						$('#procedureIcon').css('visibility', 'hidden');
					}
				}
				else if(slideIndex == 4)
				{
					if(direction == 'right')
					{
	 			 		$('#assistanceIntro').animate({right: '-130%'}, 1500, 'easeOutExpo');
						$('#assistanceIntro').css('visibility', 'hidden');
					}
					else if(direction == 'left')
					{
	 			 		$('#assistanceIntro').animate({right: '-130%'}, 1500, 'easeOutExpo');
						$('#assistanceIntro').css('visibility', 'hidden');
					}
				}
				else if(slideIndex == 5)
				{
					if(direction == 'right')
					{
	 			 		$('#imaginaireIntro').animate({left: '-130%'}, 1500, 'easeOutExpo');
						$('#imaginaireIntro').css('visibility', 'hidden');
					}
					else if(direction == 'left')
					{
	 			 		$('#imaginaireIntro').animate({left: '-130%'}, 1500, 'easeOutExpo');
						$('#imaginaireIntro').css('visibility', 'hidden');
					}
				}
			}
			else if(anchorLink == 'paysage')
			{
				if(slideIndex == 1)
				{
					if(direction == 'right')
					{
			    		$('#glisserBIntro').animate({left: '-130%'}, 1500, 'easeOutExpo');
			    		$('#glisserTIntro').animate({right: '-130%'}, 1500, 'easeOutExpo');
						$('#glisserTIntro').css('visibility', 'hidden');
			    		$('#glisserBIntro').css('visibility', 'hidden');
					}
					else if(direction == 'left')
					{
						$('#glisserBIntro').animate({left: '-130%'}, 1500, 'easeOutExpo');
			    		$('#glisserTIntro').animate({right: '-130%'}, 1500, 'easeOutExpo');
						$('#glisserTIntro').css('visibility', 'hidden');
			    		$('#glisserBIntro').css('visibility', 'hidden');
					}
				}
				else if(slideIndex == 2)
				{
					if(direction == 'right')
					{
	 			 		$('#alternanceIntro').animate({right: '-130%'}, 1500, 'easeOutExpo');
						$('#alternanceIntro').css('visibility', 'hidden');
					}
					else if(direction == 'left')
					{
	 			 		$('#alternanceIntro').animate({right: '-130%'}, 1500, 'easeOutExpo');
						$('#alternanceIntro').css('visibility', 'hidden');
					}
				}
				else if(slideIndex == 3)
				{
					if(direction == 'right')
					{
			    		$('#detailBIntro').animate({left: '-130%'}, 1500, 'easeOutExpo');
			    		$('#detailTIntro').animate({right: '-130%'}, 1500, 'easeOutExpo');
						$('#detailTIntro').css('visibility', 'hidden');
			    		$('#detailBIntro').css('visibility', 'hidden');
					}
					else if(direction == 'left')
					{
						$('#detailBIntro').animate({left: '-130%'}, 1500, 'easeOutExpo');
			    		$('#detailTIntro').animate({right: '-130%'}, 1500, 'easeOutExpo');
						$('#detailTIntro').css('visibility', 'hidden');
			    		$('#detailBIntro').css('visibility', 'hidden');
					}
				}
				else if(slideIndex == 4)
				{
					if(direction == 'right')
					{
	 			 		$('#plantationIntro').animate({right: '-130%'}, 1500, 'easeOutExpo');
						$('#plantationIntro').css('visibility', 'hidden');
					}
					else if(direction == 'left')
					{
	 			 		$('#plantationIntro').animate({right: '-130%'}, 1500, 'easeOutExpo');
						$('#plantationIntro').css('visibility', 'hidden');
					}
				}
				else if(slideIndex == 5)
				{
					if(direction == 'right')
					{
	 			 		$('#integrerIntro').animate({left: '-130%'}, 1500, 'easeOutExpo');
						$('#integrerIntro').css('visibility', 'hidden');
						$('#integrerQuoteIntro').hide();
					}
					else if(direction == 'left')
					{
	 			 		$('#integrerIntro').animate({left: '-130%'}, 1500, 'easeOutExpo');
						$('#integrerIntro').css('visibility', 'hidden');
						$('#integrerQuoteIntro').hide();

					}
				}
			}
		}
	});

	$("#doorClose1").hide();
	$("#doorOpen1").click(function() {
			$("#doorClose1").show();
			$("#doorOpen1").hide();
			$("#bafu1").css('visibility', 'visible');
			$("#bafu1").animate({marginTop: '60%'}, 1500, 'easeOutExpo');
		});	
	$("#doorClose1").click(function() {
			$("#doorClose1").hide();
			$("#doorOpen1").show();
			$("#bafu1").animate({marginTop: '0%'}, 1500, 'easeOutExpo');
			$("#bafu1").css('visibility', 'hidden');
		});	
	$("#doorClose2").hide();
	$("#doorOpen2").click(function() {
			$("#doorClose2").show();
			$("#doorOpen2").hide();
			$("#bafu2").css('visibility', 'visible');
			$("#bafu2").animate({marginTop: '60%'}, 1500, 'easeOutExpo');
		});	
	$("#doorClose2").click(function() {
			$("#doorClose2").hide();
			$("#doorOpen2").show();
			$("#bafu2").animate({marginTop: '0%'}, 1500, 'easeOutExpo');
			$("#bafu2").css('visibility', 'hidden');
		});		
	$("#doorClose3").hide();
	$("#doorOpen3").click(function() {
			$("#doorClose3").show();
			$("#doorOpen3").hide();
			$("#bafu3").css('visibility', 'visible');
			$("#bafu3").animate({marginTop: '60%'}, 1500, 'easeOutExpo');
		});	
	$("#doorClose3").click(function() {
			$("#doorClose3").hide();
			$("#doorOpen3").show();
			$("#bafu3").animate({marginTop: '0%'}, 1500, 'easeOutExpo');
			$("#bafu3").css('visibility', 'hidden');
		});	
	/*
	 * fancy box
	 */

		$("#galerie1").click(function() {
		$.fancybox.open([
			{
				href : '.JPG',
			},			
		
		], {
			helpers : {
				thumbs : {
					width: 75,
					height: 50
				}
			}
		});
	});$("#galerie2").click(function() {
		$.fancybox.open([
			{
				href : 'popup/B/A-Giratoire Aimée Marie Rossignol.JPG',
				title : 'Giratoire Aimée Marie Rossignol'
			},			
			{
				href : 'popup/B/B-CHEVIGNY SAINT SAUVEUR Giratoire ZAC Terres Rousses.JPG',
				title : 'CHEVIGNY SAINT SAUVEUR Giratoire ZAC Terres Rousses'
			},			
			{
				href : 'popup/B/C-CHEVIGNY SAINT SAUVEUR Giratoire  RD 107.JPG',
				title : 'CHEVIGNY SAINT SAUVEUR Giratoire  RD 107'
			},			
			{
				href : 'popup/B/D-MAXILLY SUR SAONE RD 976.JPG',
				title : 'MAXILLY SUR SAONE RD 976'
			},			
			{
				href : 'popup/B/E-TALANT Rue Vannerie.JPG',
				title : 'TALANT Rue Vannerie'
			},			
			{
				href : 'popup/B/F-TALANT Rue de la Libération.jpg',
				title : 'TALANT Rue de la Libération'
			},			
			{
				href : 'popup/B/G-PERRIGNY LES DIJON Rue de Prielle',
				title : 'PERRIGNY LES DIJON Rue de Prielle'
			},			
			{
				href : 'popup/B/H-TALANT Rue de Bellevue.JPG',
				title : 'TALANT Rue de Bellevue'
			},			
			{
				href : 'popup/B/I-MAXILLY SUR SAONE RD 976.JPG',
				title : 'MAXILLY SUR SAONE RD 976'
			},			
			{
				href : 'popup/B/J-I-MAXILLY SUR SAONE RD 976.JPG',
				title : 'MAXILLY SUR SAONE RD 976'
			},			
			{
				href : 'popup/B/K-BRESSEY SUR TILLE RD 107 .JPG',
				title : 'BRESSEY SUR TILLE RD 107'
			},			
			{
				href : 'popup/B/L-DIJON Boulevard Montaigne.JPG',
				title : 'DIJON Boulevard Montaigne'
			},			
			{
				href : 'popup/B/M-DIJON Rue J. MAZEN.JPG',
				title : 'DIJON Rue J. MAZEN'
			},			
			{
				href : 'popup/B/N-PERRIGNY LES DIJON Rue de Prielle.JPG',
				title : 'PERRIGNY LES DIJON Rue de Prielle'
			},			
			{
				href : 'popup/B/O-DIJON Parking du Zenith.JPG',
				title : 'DIJON Parking du Zenith'
			},
		], {
			helpers : {
				thumbs : {
					width: 75,
					height: 50
				}
			}
		});
	});
	$("#galerie3").click(function() {
		$.fancybox.open([
			{
				href : 'popup/C/A-BOURBERAIN Place.jpg',
				title : 'BOURBERAIN Place'
			},
			{
				href : 'popup/C/B-BOURBERAIN Place.jpg',
				title : 'BOURBERAIN Place'
			},
			{
				href : 'popup/C/C-GEVREY CHAMBERTIN Place du château.JPG',
				title : 'GEVREY CHAMBERTIN Place du château'
			},
			{
				href : 'popup/C/D-AHUY place du 19 mars 1962.jpg',
				title : 'AHUY place du 19 mars 1962'
			},
			{
				href : 'popup/C/E-DIJON Extérieurs Zenith.JPG',
				title : 'DIJON Extérieurs Zenith'
			},
			{
				href : 'popup/C/F-FONTAINE LES DIJON Les Champs Rémy.JPG',
				title : 'FONTAINE LES DIJON Les Champs Rémy'
			},
			{
				href : 'popup/C/G-FONTAINE LES DIJON Les Champs Rémy.JPG',
				title : 'FONTAINE LES DIJON Les Champs Rémy'
			},
			{
				href : 'popup/C/H-DIJON Le Clos de Pouilly.JPG',
				title : 'DIJON Le Clos de Pouilly'
			},
			{
				href : 'popup/C/I-TALANT Fontaine rue de la Libération.jpg',
				title : 'TALANT Fontaine rue de la Libération'
			},
			{
				href : 'popup/C/J-TALANT Coulée verte.JPG',
				title : 'TALANT Coulée verte'
			},
			{
				href : 'popup/C/K_DIJON Parc relais de Mirande.jpg',
				title : 'DIJON Parc relais de Mirande'
			},			
	
		], {
			helpers : {
				thumbs : {
					width: 75,
					height: 50
				}
			}
		});
	});
	$("#galerie4").click(function() {
		$.fancybox.open([
			{
				href : 'popup/D/D-TALANT Les Marronniers.jpg',
				title : 'TALANT Les Marronniers'
			},
			{
				href : 'popup/D/E2-FONTAINE LES DIJON Les Champs Remy après travaux.JPG',
				title : 'FONTAINE LES DIJON Les Champs Remy après travaux'
			},
			{
				href : 'popup/D/k-plan.JPG',
				title : 'K-PLAN + Coupe bassins'
			},
			{
				href : 'popup/D/j-pl.JPG',
				title : 'J-PL CANA'
			},
			{
				href : 'popup/D/I-CHENOVE Ilot Renan après les travaux.JPG',
				title : 'CHENOVE Ilot Renan après les travaux'
			},
			{
				href : 'popup/D/B- DIJON MAZEN SULLY.JPG',
				title : 'DIJON MAZEN SULLY'
			},
			{
				href : 'popup/D/A_TALANT Le Coteau des Chivalières.jpg',
				title : 'A_TALANT Le Coteau des Chivalières'
			},
			{
				href : 'popup/D/H-CHENOVE Ilot Renan pendant les travaux.jpg',
				title : 'CHENOVE Ilot Renan pendant les travaux'
			},
	
			{
				href : 'popup/D/F-DIJON Le Clos de la Providence.jpg',
				title : 'DIJON Le Clos de la Providence'
			},
	
			{
				href : 'popup/D/G-DIJON Le Clos de la Providence.jpg',
				title : 'G-DIJON Le Clos de la Providence'
			},
	
			{
				href : 'popup/D/C- DIJON Le Clos de la Providence.jpg',
				title : 'DIJON Le Clos de la Providence'
			},
	
			{
				href : 'popup/D/E1-FONTAINE LES DIJON Les Champs Remy travaux.JPG',
				title : 'FONTAINE LES DIJON Les Champs Remy travaux'
			},
	
		], {
			helpers : {
				thumbs : {
					width: 75,
					height: 50
				}
			}
		});
	});
	$("#galerie5").click(function() {
		$.fancybox.open([
			{
				href : 'popup/F/A-GEVREY CHAMBERTIN Place du Château.JPG',
				title : 'GEVREY CHAMBERTIN Place du Château'
			},
			{
				href : 'popup/F/B-QUINCY LE VICOMTE Grande Rue.JPG',
				title : 'QUINCY LE VICOMTE Grande Rue'
			},
			{
				href : 'popup/F/C-QUINCY LE VICOMTE Grande Rue.JPG',
				title : 'QUINCY LE VICOMTE Grande Rue'
			},
			{
				href : 'popup/F/D-ECHALOT Source du Brevon.jpg',
				title : 'ECHALOT Source du Brevon'
			},
			{
				href : 'popup/F/E1-FONTAINE LES DIJON Les Champs Rémy.JPG',
				title : 'FONTAINE LES DIJON Les Champs Rémy'
			},
			{
				href : 'popup/F/F-DIJON Le Chateau de Pouilly.JPG',
				title : 'DIJON Le Chateau de Pouilly'
			},
			{
				href : 'popup/F/G-IS SUR TILLE Le Prevert.JPG',
				title : 'IS SUR TILLE Le Prevert'
			},
			
			{
				href : 'popup/F/H-GEVREY CHAMBERTIN Rue de l\'Eglise.JPG',
				title : 'GEVREY CHAMBERTIN Rue de l\'Eglise'
			},
			{
				href : 'popup/F/I-ECHALOT Source du Brevon.jpg',
				title : 'ECHALOT Source du Brevon'
			},

		], {
			helpers : {
				thumbs : {
					width: 75,
					height: 50
				}
			}
		});
	});
	$("#galerie6").click(function() {
		$.fancybox.open([
			{
				href : 'popup/E/A-Champs Remy.jpg',
				title : 'Champs Remy'
			},
			{
				href : 'popup/E/B-Chivallières.jpg',
				title : 'Chivallières'
			},

			{
				href : 'popup/E/C-Josset.jpg',
				title : 'Josset'
			},

			{
				href : 'popup/E/D-pouilly.jpg',
				title : 'Pouilly'
			},

			{
				href : 'popup/E/E-prevert.jpg',
				title : 'Prevert'
			},
			{
				href : 'popup/F/charme du petit bois.JPG',
				title : 'Charmes du petit bois'
			},

			{
				href : 'popup/E/G-SIte SEITA.jpg',
				title : 'Site SEITA'
			},

			{
				href : 'popup/E/H-Cestres.jpg',
				title : 'Cestres'
			},
			{
				href : 'popup/F/mesmont.JPG',
				title : 'Mesmont'
			},

			{
				href : 'popup/F/saint julien.JPG',
				title : 'Saint Julien'
			},

			{
				href : 'popup/E/K-DSC_0007.JPG'
			},

			{
				href : 'popup/E/L-IMG_9699.JPG'
			},

			{
				href : 'popup/E/M-.JPG'
			},

			{
				href : 'popup/E/N-DSC_0040.JPG'
			},

		], {
			helpers : {
				thumbs : {
					width: 75,
					height: 50
				}
			}
		});
	});


	/* MENU ------------------------------------------------------------------------------ */
	$('.submenuBafu').hide();
	$('.submenuCir').hide();
	$('.submenuEspace').hide();
	$('.submenuHydrau').hide();
	$('.submenuUrba').hide();
	$('.submenuPaysage').hide();

					

	$('.menuBafu').on('click', function(){
		$('.submenuBafu').toggle("slow");
		$('.submenuCir').delay(1).hide("slow");
		$('.submenuEspace').delay(1).hide("slow");
		$('.submenuHydrau').delay(1).hide("slow");
		$('.submenuUrba').delay(1).hide("slow");
		$('.submenuPaysage').delay(1).hide("slow");
	});

	$('.menuCir').on('click', function(){
		$('.submenuCir').toggle("slow");
		$('.submenuBafu').delay(1).hide("slow");
		$('.submenuEspace').delay(1).hide("slow");
		$('.submenuHydrau').delay(1).hide("slow");
		$('.submenuUrba').delay(1).hide("slow");
		$('.submenuPaysage').delay(1).hide("slow");
	});

	$('.menuEspace').on('click', function(){
		$('.submenuEspace').toggle("slow");
		$('.submenuBafu').delay(1).hide("slow");
		$('.submenuCir').delay(1).hide("slow");
		$('.submenuHydrau').delay(1).hide("slow");
		$('.submenuUrba').delay(1).hide("slow");
		$('.submenuPaysage').delay(1).hide("slow");
	});

	$('.menuHydrau').on('click', function(){
		$('.submenuHydrau').toggle("slow");
		$('.submenuBafu').delay(1).hide("slow");
		$('.submenuCir').delay(1).hide("slow");
		$('.submenuEspace').delay(1).hide("slow");
		$('.submenuUrba').delay(1).hide("slow");
		$('.submenuPaysage').delay(1).hide("slow");
	});

	$('.menuUrba').on('click', function(){
		$('.submenuUrba').toggle("slow");
		$('.submenuBafu').delay(1).hide("slow");
		$('.submenuCir').delay(1).hide("slow");
		$('.submenuEspace').delay(1).hide("slow");
		$('.submenuHydrau').delay(1).hide("slow");
		$('.submenuPaysage').delay(1).hide("slow");
	});

	$('.menuPaysage').on('click', function(){
		$('.submenuPaysage').toggle("slow");
		$('.submenuBafu').delay(1).hide("slow");
		$('.submenuCir').delay(1).hide("slow");
		$('.submenuEspace').delay(1).hide("slow");
		$('.submenuHydrau').delay(1).hide("slow");
		$('.submenuUrba').delay(1).hide("slow");
	});

});
